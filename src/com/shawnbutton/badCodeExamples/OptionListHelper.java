package com.shawnbutton.badCodeExamples;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class OptionListHelper {

	public static List<SelectOption> checkEndMonthList(Date expiryDate) {
		List<SelectOption> monthList = new ArrayList<SelectOption>();
        SimpleDateFormat formatm = new SimpleDateFormat("MM");
        int month = Integer.parseInt((formatm.format(expiryDate)));
		for (int intLooper = 1; intLooper <= 12; intLooper++) {
			SelectOption option = new SelectOption();
			if (month == intLooper) {
				option.setLabel(String.valueOf(intLooper));
				option.setValue(String.valueOf(intLooper));
				option.setSelected(true);
			} else {
				option.setLabel(String.valueOf(intLooper));
				option.setValue(String.valueOf(intLooper));
				option.setSelected(false);
			}
			monthList.add(option);
		}
		return monthList;
	}

	public static List<SelectOption> createEndDaysList(Date currentDate) {
		List<SelectOption> daysList = new ArrayList<SelectOption>();
        Date endDt = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
            String selDt = dateFormat.format(currentDate);
            DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            Date dt1 = (Date) formatter.parse(selDt);

            Calendar cal = Calendar.getInstance();
            cal.setTime(dt1);
            cal.add(Calendar.DATE, 14);

            endDt = cal.getTime();

        } catch (ParseException e) {
            // Problem adding days to date
        }
        Date dt = endDt;
        SimpleDateFormat formatd = new SimpleDateFormat("dd");
        int day = Integer.parseInt((formatd.format(dt)));
		for (int intLooper = 1; intLooper <= 31; intLooper++) {
			SelectOption option = new SelectOption();
			if (day == intLooper) {
				option.setLabel(String.valueOf(intLooper));
				option.setValue(String.valueOf(intLooper));
				option.setSelected(true);
			} else {
				option.setLabel(String.valueOf(intLooper));
				option.setValue(String.valueOf(intLooper));
				option.setSelected(false);
			}
			daysList.add(option);
		}
		return daysList;
	}

	public static List<SelectOption> checkEndDaysList(Date expiryDate) {
		List<SelectOption> daysList = new ArrayList<SelectOption>();
        SimpleDateFormat formatd = new SimpleDateFormat("dd");
        int day = Integer.parseInt((formatd.format(expiryDate)));
		for (int intLooper = 1; intLooper <= 31; intLooper++) {
			SelectOption option = new SelectOption();
			if (day == intLooper) {
				option.setLabel(String.valueOf(intLooper));
				option.setValue(String.valueOf(intLooper));
				option.setSelected(true);
			} else {
				option.setLabel(String.valueOf(intLooper));
				option.setValue(String.valueOf(intLooper));
				option.setSelected(false);
			}
			daysList.add(option);
		}
		return daysList;
	}
		
}
